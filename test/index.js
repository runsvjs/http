'use strict';
const assert = require('assert');
const runsv = require('runsv');
const http = require('..');
const request = require('supertest');
const {Server} = require('http');

function withRunsv(fn){
	before(function (){
		const self = this;
		const sv = runsv.create();
		self.sv = sv;
	});
	fn();
	after(function(done){
		this.sv.stop(done);
	});
}
describe('runsv-http wrapper', function(){
	describe('#create([name])',function(){
		describe('requestListener specified via #createServer',function(){
			withRunsv(function(){
				before(function (done){
					const self = this;
					const {sv} = self;
					function requestListener(req, res){
						res.end('hello');
					}
					const web = http().createServer(requestListener).listen();
					sv.addService(web);
					sv.start(function(err, clients){
						if(err){
							return done(err);
						}
						const {http} = clients;
						self.http = http;
						return done();
					});
				});
			});
			it('should create http server from requestListener', function(done){
				const {http} = this;
				assert(http instanceof Server, 'client is not an instance of http.Server');
				const {port} = http.address();
				const url = `http://localhost:${port}`;
				request(url)
					.get('/')
					.expect(200)
					.expect('hello')
					.end(done);
			});
		});
	});

	describe('service name', function(){
		it('default name should be "http"',function(){
			const app = (req, res) => res.end();
			const web = http().createServer(app).listen();
			assert.deepStrictEqual(web.name, 'http');
		});
		it('should be possible to specify a service name',function(){
			const app = (req, res) => res.end();
			const web = http('myweb').createServer(app).listen();
			assert.deepStrictEqual(web.name, 'myweb');
		});
	});

	describe('On late setup', function(){
		describe('#createServer(...)',function(){
			before(function start(done){
				this.service = http();
				this.service.start(null, done);
			});
			after(function stop(done){
				this.service.stop(done);
			});
			it('should fail', function(done){
				const {service} = this;
				try{
					service.createServer();
				}catch(e){
					assert.deepStrictEqual(e.message, 'already setup');
					return done();
				}
				return done(new Error('unexpected code branch'));
			});
		});

		describe('#listen(...)', function(){
			before(function start(done){
				this.service = http().createServer();
				this.service.start(null, done);
			});
			after(function stop(done){
				this.service.stop(done);
			});
			it('should fail',function(done){
				const {service} = this;
				try{
					service.listen();
				}catch(e){
					assert.deepStrictEqual(e.message, 'already setup');
					return done();
				}
				return done(new Error('unexpected code branch'));
			});
		});
	});

	describe('Prevent #listen callback', function(){
		it('should fail', function(){
			const expected = 'listen(...callback) should not be included';
			const noop = () => {};
			try{
				http().createServer().listen(noop);
			}catch(e){
				const actual = e.message;
				assert.deepStrictEqual(actual, expected);
				return;
			}
			throw new Error('unexpected code branch');
		});
	});
	describe('#start()', function(){
		describe('When a client has been created', function(){
			before(function setup(done){
				function requestListener(req, res){
					res.end('hello');
				}
				const web = http().createServer(requestListener).listen();
				this.web = web;
				web.start(null, done);
			});
			after(function stop(done){
				this.web.stop(done);
			});
			it('should not replace existing client if called again',function(done){
				const {web} = this;
				web.start(null, function(err){
					assert.deepStrictEqual(err.message, 'already started');
					return done();
				});
			});
		});
	});
	describe('#stop(callback)', function(){
		it('should not break if it has never been started', function(done){
			http().stop(done);
		});
	});
});
