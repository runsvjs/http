[![pipeline status](https://gitlab.com/runsvjs/http/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/http/commits/master)
[![coverage report](https://gitlab.com/runsvjs/http/badges/master/coverage.svg)](https://gitlab.com/runsvjs/http/commits/master)

# runsv-http

A runsv wrapper around [http.Server](https://nodejs.org/dist/latest/docs/api/http.html#http_class_http_server).

## Usage

### Simple express app

```javascript

const runsv = require('runsv').create();
const createHTTPService = require('runsv-http');
const express = require('express');
const app = express();
const PORT = 3000;

/* set up your app */
app.get('/', (req, res) => res.end('hello'));

// configure the runsv-http service
const myWeb = createHTTPService('myWeb') // optionally you can set a custom name
	.createServer(app)  // same signature as http.createServer()
	.listen(PORT); // same signature as server.listen. DO NOT include the callback.

runsv.addService(myWeb);
runsv.start(function(err){
	const {myWeb} = runsv.getClients();
	console.log(`listening at port ${PORT}`);
});
//...
```

## API

In node you usually create an instance of an http server like this:
```javascript
const http = require('http');
const port = 3000;
function listener(req, res){
	res.end('hello');
}
const server = http.createServer(listener);
server.listen(port, function(err){
	console.log(`listening at ${port}`);
});
```

Both `createServer()` and `listen()` accept optional params. 
The goal of this API is to be as close as possible to the original node API.  

```javascript
const runsv = require('runsv').create();
const port = 3000;
const createService = require('runsv-http');

function listener(req, res){
	res.end('hello');
}

const server = createService()
	.createServer(listener)
	.listen(port);

runsv.addService(server);
runsv.start(function(err){
	console.log(`listening at ${port}`);
});
//...
```


* `#createService([name])` returns a `runsv` service wrapper around original nodejs`http.server`.
	* `[name='http']` runsv service name. Optional. By default is **http**. 
	* Returns: a `runsv` service. This service also exposes `#createServer([options][,requestListener])` to mimic original node API
* `#createServer([options][,requestListener]` mimics the [original function](https://nodejs.org/dist/latest/docs/api/http.html#http_http_createserver_options_requestlistener)
	* Returns: previous `runsv` service plus the `#listen()` function.
* `#listen()` check the options of the [original function](https://nodejs.org/dist/latest/docs/api/net.html#net_server_listen). Do not include the callback instead, use `runsv` [start](https://gitlab.com/runsvjs/runsv#events) event.
	* Returns: previous `runsv` service.

## Examples

### Random port

```javascript
const runsv = require('runsv').create();
const createService = require('runsv-http');

function listener(req, res){
	res.end('hello');
}

const server = createService()
	.createServer(listener);

runsv.addService(server);
runsv.start(function(err){
	const {port} = services.getClients().http.address();
	console.log(`listening at ${port}`);
});
//...
```

### Handle on request
```javascript
const runsv = require('runsv').create();
const createService = require('runsv-http');

function listener(req, res){
	res.end('hello');
}

// As with the original http service we can define 
// a listener later...
const server = createService();

runsv.addService(server);
runsv.start(function(err){
	const {http} = services.getClients();
	// Respond to request events
	http.on('request', listener);
	const {port} = http.address();
	console.log(`listening at ${port}`);
});
//...
```

[See more examples](./examples)

## Stop behavior

`#stop(callback)` just invokes [server.close([callback])](https://nodejs.org/api/net.html#net_server_close_callback). 

Bear in mind that stopping (or closing) this service **will not terminate** the http server immediately.  
[http-terminator](https://github.com/gajus/http-terminator#behaviour) docs explains why.

> When you call server.close(), it stops the server from accepting new connections, but it keeps the existing connections open indefinitely. This can result in your server hanging indefinitely due to keep-alive connections or because of the ongoing requests that do not produce a response. Therefore, in order to close the server, you must track creation of all connections and terminate them yourself.
>
> -- <cite><a href="https://github.com/gajus/http-terminator#behaviour">http-terminator docs</a></cite>
Tracking the connections and terminating them in a gracefully way is beyond the scope of this service wrapper.  
There are modules like [http-terminator](https://github.com/gajus/http-terminator), [http-graceful-shutdown](https://github.com/sebhildebrandt/http-graceful-shutdown) or [http-close](https://www.npmjs.com/package/http-close) that will do the heavy lifting for you.

```javascript
// Example: Gracefully terminating connections with http-close
const runsv = require('runsv').create();
const createHTTPService = require('runsv-http');
const httpClose = require('http-close');
const express = require('express');
const app = express();

app.get('/', (req, res) => res.end('hello'));

const httpService = createHTTPService('server').createServer(app);

runsv.addService(httpService);
runsv.start(function(err){
	const {server} = runsv.getClients();
	// modify server.close behavior to gracefully terminate connections
	httpClose({ timeout: 2000 }, server);
});
//...
```
