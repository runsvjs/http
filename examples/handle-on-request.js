'use strict';

const runsv = require('runsv');
const createHttpServiceWrapper = require('..');
const services = runsv.create();
const express = require('express');
const app = express();

app.get('/', function(req, res){
	res.end('handle on request, no app setup');
});

const server = createHttpServiceWrapper();

// configure future http.server
services.addService(server);

services.start(function(err, {http}){
	if(err){
		console.error('could not start:', err.message);
		console.trace(err.stack);
		services.stop(err);
	}
	const {port} = services.getClients().http.address();
	http.on('request', app);

	console.log(`listening at ${port}`);
});

process.on('SIGINT', function(){
	console.error('ctrl + c');
	services.stop(() => console.log('done'));
});
