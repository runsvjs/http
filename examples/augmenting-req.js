'use strict';

const runsv = require('runsv');
const createHttpServiceWrapper = require('..');
const services = runsv.create();
const express = require('express');
const http = require('http');
const req = http.IncomingMessage.prototype;
const app = express();

const superdb = {
	get(id, callback){
		return callback(null, {id});
	}
};

app.get('/', function(req, res){
	req.db.get(1, function(err, data){
		res.json(data);
	});
});

const server = createHttpServiceWrapper('http').createServer(app);

// configure future http.server
services.addService(server);

services.start(function(err){
	if(err){
		console.error('could not start:', err.message);
		console.trace(err.stack);
		services.stop(err);
	}
	req.db = superdb;
	const {port} = services.getClients().http.address();

	console.log(`listening at ${port}`);
});

process.on('SIGINT', function(){
	console.error('ctrl + c');
	services.stop(() => console.log('done'));
});
