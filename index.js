'use strict';
const http = require('http');

function throwIfLastParamIsACallback(list, message){
	const lastParam = list[list.length -1];
	if(typeof lastParam === 'function'){
		throw new Error(message);
	}
}
function createService(name='http'){
	let server;
	let args = {
		createServer : [],
		listen: []
	};
	let ret = {
		name,
		start(_, callback){
			if(server){
				return callback(new Error('already started'));
			}
			server = http.createServer(...args.createServer);
			server.listen(...args.listen, callback);
		},
		stop(callback){
			if(!server){
				return callback();
			}
			server.close(callback);
		},
		getClient(){
			return server;
		}
	};
	function throwIfLateSetup(){
		if(server){
			throw new Error('already setup');
		}
	}
	// always return a service.
	// calling #createServer or #listen will just 
	// add more config
	return {...ret, 
		createServer(...options){
			throwIfLateSetup();
			args.createServer = options;
			return {...ret,
				listen(...options){
					throwIfLastParamIsACallback(options,'listen(...callback) should not be included');
					throwIfLateSetup();
					args.listen = options;
					return ret;
				}
			};
		}
	};
}
exports = module.exports = createService;
